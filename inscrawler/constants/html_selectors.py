# HTML Classes
# Profile

PROFILE_NAME = "._aacl._aacp._aacw._aacx._aad7._aade"
PROFILE_DESCRIPTION = "._aa_c > ._aacl._aacp._aacu._aacx._aad6._aade"
PROFILE_PUBLIC_ACCOUNT_PHOTO = "._aarf > ._aa8h > ._aa8j"
PROFILE_PRIVATE_ACCOUNT_PHOTO = "._aadp"
PROFILE_STATISTICS = "._ac2a"
PROFILE_FOLLOWERS_ELEMENTS = ".RnEpo.Yx5HN .isgrP .PZuss .FPmhX"  # check if it can be changed to ".Jv7Aj.MqpiF"

FOLLOWERS_LAST_PROFILE = ".wo9IH:last-child .enpQJ a"

# JS Scripts

FOLLOWERS_SCROLL_DOWN = "document.querySelector(\".isgrP\").scrollTo(0, document.querySelector(" \
                        "\".isgrP\").scrollHeight); "
