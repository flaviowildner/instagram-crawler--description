from __future__ import unicode_literals

import glob
import os
import time
from builtins import open

from .browser import Browser
from .constants import consts
from .constants.html_selectors import PROFILE_NAME, PROFILE_DESCRIPTION, PROFILE_PUBLIC_ACCOUNT_PHOTO, \
    PROFILE_PRIVATE_ACCOUNT_PHOTO, PROFILE_STATISTICS
from .exceptions import RetryException
from .model.profile import Profile
from .utils import retry


class Logging(object):
    PREFIX = "instagram-crawler"

    def __init__(self):
        try:
            timestamp = int(time.time())
            self.cleanup(timestamp)
            self.logger = open("/tmp/%s-%s.log" %
                               (Logging.PREFIX, timestamp), "w")
            self.log_disable = False
        except Exception:
            self.log_disable = True

    def cleanup(self, timestamp):
        days = 86400 * 7
        days_ago_log = "/tmp/%s-%s.log" % (Logging.PREFIX, timestamp - days)
        for log in glob.glob("/tmp/instagram-crawler-*.log"):
            if log < days_ago_log:
                os.remove(log)

    def log(self, msg):
        if self.log_disable:
            return

        self.logger.write(msg + "\n")
        self.logger.flush()

    def __del__(self):
        if self.log_disable:
            return
        self.logger.close()


class InsCrawler(Logging):
    URL = "https://www.instagram.com"
    RETRY_LIMIT = 10

    def __init__(self, has_screen: bool = False):
        super(InsCrawler, self).__init__()
        self.browser = Browser(has_screen)
        self.page_height = 0

    def _dismiss_login_prompt(self):
        ele_login = self.browser.find_one(".Ls00D .Szr5J")
        if ele_login:
            ele_login.click()

    def login(self):
        browser = self.browser
        url = "%s/accounts/login/" % InsCrawler.URL
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(consts.USERNAME)
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys(consts.PASSWORD)

        login_btn = browser.find_one(".L3NKy")
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()

    def get_user_profile(self, username: str) -> Profile:
        browser = self.browser
        url = "%s/%s/" % (InsCrawler.URL, username)
        browser.get(url)

        name: str = self.__get_profile_name()
        description: str = self.__get_profile_description()
        photo: str = self.__get_photo_url()

        statistics_elem = self.browser.find(PROFILE_STATISTICS)
        post_num, follower_num, following_num = self.get_user_statistics(statistics_elem)

        return Profile(username=username, name=name, description=description, n_followers=follower_num,
                       n_following=following_num, n_posts=post_num, followers=[], followings=[],
                       photo_url=photo)

    def get_user_statistics(self, statistics) -> (int, int, int):
        post_num: int = int(statistics[0].text.replace(",", "").replace(".", ""))
        follower_num: int = int(statistics[1].get_attribute("title").replace(",", "").replace(".", ""))
        following_num: int = int(statistics[2].text.replace(",", "").replace(".", ""))
        return post_num, follower_num, following_num

    def __get_photo_url(self):
        try:
            photo = self.browser.find_one(PROFILE_PUBLIC_ACCOUNT_PHOTO).get_attribute("src")
        except AttributeError:
            try:
                photo = self.browser.find_one(PROFILE_PRIVATE_ACCOUNT_PHOTO).get_attribute("src")  # Private profile
            except AttributeError:
                photo = ''
        return photo

    def __get_profile_description(self):
        try:
            desc = self.browser.find_one(PROFILE_DESCRIPTION).text
        except AttributeError:
            desc = ''
        return desc

    def __get_profile_name(self):
        try:
            name = self.browser.find_one(PROFILE_NAME).text
        except AttributeError:
            name = ''
        return name
